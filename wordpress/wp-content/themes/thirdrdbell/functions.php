<?php
/**
 * thirdrdbell functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package thirdrdbell
 */

//global $mysqlUsername=root;
//global mysqlPassword='';

global $newdb;
 $newdb= new wpdb("root","", "3rdbellApi","localhost");

if ( ! function_exists( 'thirdrdbell_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function thirdrdbell_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on thirdrdbell, use a find and replace
	 * to change 'thirdrdbell' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'thirdrdbell', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'thirdrdbell' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'thirdrdbell_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'thirdrdbell_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function thirdrdbell_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'thirdrdbell_content_width', 640 );
}
add_action( 'after_setup_theme', 'thirdrdbell_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function thirdrdbell_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'thirdrdbell' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'thirdrdbell' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'thirdrdbell_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function thirdrdbell_scripts() {
	wp_enqueue_style( 'thirdrdbell-style', get_stylesheet_uri() );

	wp_enqueue_script( 'thirdrdbell-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'thirdrdbell-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'thirdrdbell_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/* Code by abir */

function create_video_post_type() {

			$labels = array(
				'name' =>'Video',
				'singular_name' =>'video',
				'add_new' =>'Add Video',
				'all items' =>'All items',
				'add_new_item' =>'Add Video',
				'edit' =>'Edit Video',
				'view' =>'View Video',
			);

	       $args=array(
                'labels'=>$labels,
                'public'=>true,
                'rewrite'=>true,
			    'supports' => array( 'title', 'editor', 'thumbnail'),
			    'taxonomies' => array( 'category', 'post_tag' ),
			    'menu_position' => 5,
			    'menu_icon'=>'dashicons-video-alt',
			    'register_meta_box_cb' => 'add_videos_metaboxes'

		   );

	       register_post_type('videos',$args);

}
add_action('init','create_video_post_type' );

add_action( 'add_meta_boxes', 'add_videos_metaboxes');


function add_videos_metaboxes() {

	add_meta_box(
		'video_type', //id
		'Video Type', //title
		'video_type', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'content_type', //id
		'Content Type', //title
		'content_type', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'video_url', //id
		'Video URL', //title
		'video_url', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'producer', //id
		'Producer', //title
		'video_producer', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'production-house', //id
		'Production House', //title
		'video_production_house', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'director', //id
		'Director', //title
		'video_director', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'author', //id
		'Author', //title
		'video_author', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'artist', //id
		'Artists', //title
		'video_artist', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'writer', //id
		'Writer', //title
		'video_writer', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'singer', //id
		'Singer', //title
		'video_singer', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'cost', //id
		'Cost', //title
		'video_cost', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'point', //id
		'Point', //title
		'video_point', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'territory', //id
		'Territory', //title
		'video_territory', //callbacks
		'videos', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'Poster Image', //id
		'Poster Image', //title
		'poster_image', //callbacks
		'videos', //page
		'side', //position of the div, i.e. (side)
		'default' //priority
	);

}

function content_type() {

	global $post;
    global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM serials";
	$serials=$newdb->get_results($query);

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Echo out the field
	echo '<select class="widefat" id="videoType" name="content_type">';
	echo '<option value="Single">Single</option>';
	echo '<option value="Serial">Serial</option>';
	echo '</select>';
	?>

	<div id="dvDynamic" class="widefat" style="display: none">

		<div class="text-center">
			<label>Select Serial</label>
		</div>

		<div class="text-right">
			<label class="cd-label" for="serials"><a href="#" id="addSerial" class="btn btn-success btn-circle btn-xs "><i class="fa fa-plus"></i>Add New Serial</a></label>
		</div>

		<select id="selectpickerSerial" class="selectpickerSerial" name="video_serial" data-live-search="true" >


			<?php foreach($serials as $key=>$serial){?>
				<option value=" <?php echo $serial->id; ?>"><?php echo $serial->name; ?></option>
			<?php };

			?>

		</select>

	</div>


	<div class="modal fade" id="serialModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Serial</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="serial_name" name="" placeholder="Enter Title">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default" id="serialSave">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<script>

		var $ =jQuery.noConflict();
		$(document).ready(function(){

			$('.selectpickerSerial').selectpicker({

				style: 'btn-warning',
				//size: 4
			});

			$(document).on('click','#addSerial',function(e) {
				e.preventDefault();
				$('#serialModal').modal('show');
			});

			$('#videoType').on('change',function(){

				var value=$(this).val();

				dynamicOptions(value);

			});


			function dynamicOptions(value){

				if(value=="Serial"){
					//console.log(value);
					$("#dvDynamic").show();
				}else{
					$("#dvDynamic").hide();
					var element = document.getElementById('selectpickerSerial');
					element.value = 0;
				}

			}

			$('#serialSave').on('click',function(e){
				e.preventDefault();
				var name=$('#serial_name').val();

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_serial',
						name : name
					},
					success: function( id ) {

						$('#serialModal').modal('hide');
						ajaxDataHandler(name,id);

					}
				})
			})


			function ajaxDataHandler(name,id){

				var picker = document.getElementById("selectpickerSerial");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				option.selected = true;
				picker.add(option);
				$('.selectpickerSerial').selectpicker('refresh');

			}
		});

	</script>


	<?php

}

add_action( 'wp_ajax_post_add_serial', 'save_Serial_On_The_Fly' );

function save_Serial_On_The_Fly() {

	global   $newdb;
	$data=$_POST['name'];
	$entry_id=wp_insert_post( array(
		'post_title'        => $data,
		'post_type'         => 'serials',
		'post_author'       => '1',
		'post_status' => 'publish',
	) );

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");

	$newdb->query($newdb->prepare(" INSERT INTO serials (post_id,name)
                                       VALUES (%d,%s)",
		array(
			$entry_id,
			$data,
		)));

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $newdb->insert_id;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

function video_type() {


	global $post;

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the location data if its already been entered
	$video_type = get_post_meta($post->ID, 'video_type', true);

	// Echo out the field
	echo '<select class="widefat" id="" name="video_type">';
	echo '<option value="'.$video_type.'" selected>'.$video_type.'</option>';
	echo '<option value="Public">Public</option>';
	echo '<option value="Subscriber Only">Subscriber Only</option>';
	echo '<option value="Paid">Paid</option>';
	echo '</select>';

}


function video_url() {

	require(get_template_directory()."/vendor/vimeo/vimeo-api/autoload.php");
	$client_identifier = 'd69e92c8f373e65c6373451df4ba956cf96d2ba7';
	$client_secrets = 'DidG0ITAV+9E3DT3TtGeR5LDIecUlhRna69mz5mJzipXeH5yn4f95hg0k74OwQUDLIHxW9FXKUKoT7FYOtETyg+P2IZ6DTZLPcSCW1CwSI58qvkVeF+N/5FhIQf/iciS';
	$lib = new \Vimeo\Vimeo($client_identifier, $client_secrets);

	$lib->setToken('06288c519c968c834ea185b0ba3e5658');
	$response = $lib->request('/me/videos', 'GET');

	$totalVideos=$response['body']['total'];
	$limit=ceil($totalVideos/25);

    $result=array();
    for($i=1;$i<=$limit;$i++){
      $response = $lib->request('/me/videos',['page'=>$i], 'GET');
      $result[''.$i.'']=$response['body']['data'];
    }


	global $post;
	global   $newdb;
    //$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT link FROM videos WHERE post_id=$post->ID";
	$link=$newdb->get_var($query);
    $url = $link;
    $newUrl = str_replace('https://vimeo.com/', 'https://player.vimeo.com/video/', $url);

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the location data if its already been entered
	$video_url = get_post_meta($post->ID, 'video_url', true);

     ?>

	<select id="selectpickerVimeo" class="selectpickerVimeo" name="video_url" data-live-search="true" >


		<?php foreach($result as $key=>$videos)
		{
		  foreach($videos as $video)
		  {
		?>
			<option data-content="<img src='<?php echo $video['pictures']['sizes'][0]['link']; ?>'> <?php echo $video['name']; ?>" value=" <?php echo $video['link']; ?>" data-width="75%">
				<?php echo $video['name']; ?></option>
		<?php
		  }
		};
		?>

	</select>

	<iframe style="float: right;" id="videoVim" src="<?php echo $newUrl ?>" width="420" height="215" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></br>

	<canvas id="canvas"></canvas> <br/><br/>

	<script>

		var $ =jQuery.noConflict();
		$(document).ready(function(){

			$('.selectpickerVimeo').selectpicker({
				style: 'btn-warning',
			});

			$('.selectpickerVimeo').change(function(){

                var vidLink=$( "#selectpickerVimeo option:selected" ).val();
				//console.log(vidLink);
				var video = document.getElementById('videoVim');
			    url = vidLink;
                var modifiedUrl=url.replace("https://vimeo.com", "https://player.vimeo.com/video");
			    //console.log(modifiedUrl)
			    video.src=modifiedUrl;
			})

		});

	</script>

	<?php

}

function video_territory() {

	global $post;

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the location data if its already been entered
	$video_territory = get_post_meta($post->ID, 'video_territory', true);

	// Echo out the field
	echo '<input type="text" name="video_territory" value="' . $video_territory  . '" class="widefat" />';

}

function video_cost() {

	global $post;

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the location data if its already been entered
	$video_cost = get_post_meta($post->ID, 'video_cost', true);

	// Echo out the field
	echo '<input type="text" name="video_cost" value="' . $video_cost  . '" class="widefat" />';

}

function video_point() {

	global $post;

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the location data if its already been entered
	$video_point = get_post_meta($post->ID, 'video_point', true);
   //var_dump($post->ID);
	// Echo out the field
	echo '<input type="text" name="video_point" value="' . $video_point  . '" class="widefat" />';

}

function video_producer() {

	global $post;
	global   $newdb;
	$producer=get_post_meta($post->ID, 'video_producer',true);

	//var_dump($post->ID);
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT galleries.image,persons.id,persons.name FROM persons LEFT JOIN galleries ON persons.post_id=galleries.person_id
              GROUP BY persons.post_id";
	$persons=$newdb->get_results($query);
	//var_dump($persons);die;
	?>

	<div class="text-right">
		<label class="cd-label" for="producers"><a href="#" id="addProducer" class="btn btn-success btn-circle btn-xs "><i class="fa fa-plus"></i>Add New Producer</a></label>
	</div>

	<div class="">
		<select id="selectpickerProducer" class="selectpickerProducer" name="video_producer[]" multiple data-selected-text-format="count > 3" data-live-search="true">


			<?php foreach($persons as $key=>$person){?>
				<option data-image="<?php echo $person->image; ?>" value=" <?php echo $person->id; ?>"><?php echo $person->name; ?></option>
			<?php };

			?>

		</select>
	</div>

	<div id="dynamicProducer" class="text-center">

		<Label id="dynamicLabelProducer" style="font-size: large"></Label>

	</div>

	<div class="modal fade" id="producerModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Producer</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="producer_name" name="" placeholder="Enter Title">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default" id="producerSave">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function() {

			$(document).on('click','#addProducer',function(e) {
				e.preventDefault();
				$('#producerModal').modal('show');
			});

			$('#producerSave').on('click',function(e){
				e.preventDefault();
				var name=$('#producer_name').val();
				//console.log(name);
				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_producer',
						name : name,
						postId: <?php echo json_encode($post->ID);?>
					},
					success: function( id ) {

						$('#producerModal').modal('hide');
						ajaxDataHandler(name,id);

					}
				})
			})

			function ajaxDataHandler(name,id){

				var Div = $('#dynamicProducer');

				$('<p><label class="cd-label text-right " for="producers"></label>' +
					' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg producerData" data-id"'+id+'">' +name+ '</i>' +
					'</p>').appendTo(Div);

				var picker = document.getElementById("selectpickerProducer");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				option.selected = true;
				picker.add(option);
				$('.selectpickerProducer').selectpicker('refresh');

				updateOtherPickersFromProducer(name,id);
			}

			$('.selectpickerProducer').selectpicker({
				style: 'btn-info',
				size: 4
			});

			$('.selectpickerProducer').change(function () {

				var result = [];
				updateProducerList();
				var Div = $('#dynamicProducer');
				var selectedText = $(this).val();

				for (var i = 0; i < selectedText.length; i++) {

					var val = selectedText[i];
					var txt = $(".selectpickerProducer option[value='"+val+"']").text();
					var image = $(".selectpickerProducer option[value='"+val+"']").data('image');

					result.push({
						'personId': val ,
						'personName': txt,
						'image':image
					});
				}

				var i=result.length;

				$('#dynamicLabelProducer').html('Selected Producers <i class="fa fa-chevron-circle-down"></i>')

				for(var j=0;j<i;j++)
				{
					$('<p><label class="cd-label text-right " for="producers"></label>' +
						'<i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg producerData" data-id="'+result[j].personId+'">' + result[j].personName + '</i>' +
						'<span style="margin-left:10px"><img id="person-image-producer" class="img-circle" alt="" width="100px" height="100px" src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/'+result[j].image+'" /></span></p>').appendTo(Div);
				}

				$("img").error(function () {
					$(this).unbind("error").attr("src", "<?php bloginfo('template_url');?>/jQuery.filer/uploads/photo_not_available.png");
				});

			});

			function updateProducerList(){
				$("#dynamicProducer p").remove();
			}

			$('#dynamicProducer').on('click','.producerData', function() {

				$(this).parent().remove();
				var value=$(this).data('id');

				$('.selectpickerProducer').find("option[value='"+value+"']").prop('selected', false);
				var values = $('.selectpickerProducer').val();

				$('.selectpickerProducer').selectpicker('val', values );

				if(values==null){
					$('#dynamicLabelProducer').html('No Producers Selected<i class="fa fa-users"></i>')
				}

			});

			function updateOtherPickersFromProducer(name,id){

				var picker = document.getElementById("selectpickerWriter");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerWriter').selectpicker('refresh');

				var picker = document.getElementById("selectpickerSinger");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerSinger').selectpicker('refresh');

				var picker = document.getElementById("selectpickerArtist");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerArtist').selectpicker('refresh');

				var picker = document.getElementById("selectpickerAuthor");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerAuthor').selectpicker('refresh');

				var picker = document.getElementById("selectpickerDirector");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerDirector').selectpicker('refresh');

			}

		});

	</script>

	<?php

}

add_action( 'wp_ajax_post_add_producer', 'save_Producer_On_The_Fly' );

function save_Producer_On_The_Fly() {

	$postId=$_POST['postId']+1;
	$data=$_POST['name'];

	$entry_id = wp_insert_post(array (
		'post_type' => 'persons',
		'post_title' => $data,
		'post_status' => 'publish',
	));

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	global   $newdb;
	$newdb->query($newdb->prepare(" INSERT INTO persons (post_id,name,created_at)
                                       VALUES (%d,%s,%s)",
		array(
			$entry_id,
			$data,
			current_time('mysql'),
		)));
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $newdb->insert_id;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}


function video_production_house() {

	global $post;
	global   $newdb;
	$type = get_post_meta($post->ID, 'video_production_house',true);
	$args      = array('post_type' => 'production_house', 'posts_per_page' => -1);
	$loop      = new WP_Query($args);
	$temp=$post;

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM production_houses";
	$p_houses=$newdb->get_results($query);

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	?>

	<select id="selectpickerProductionHouse" class="selectpickerProductionHouse" name="video_production_house" data-live-search="true" >


		<?php foreach($p_houses as $key=>$p_house){?>
			<option value=" <?php echo $p_house->id; ?>"><?php echo $p_house->name; ?></option>
		<?php };

		?>

	</select>


	<script>

		var $ =jQuery.noConflict();
		$(document).ready(function(){

			$('.selectpickerProductionHouse').selectpicker({
				style: 'btn-warning',
			});

		});

	</script>
	<?php
}

function video_director() {

	global $post;
	global   $newdb;
	$director=get_post_meta($post->ID, 'video_director',true);

	//var_dump($post->ID);
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT galleries.image,persons.id,persons.name FROM persons LEFT JOIN galleries ON persons.post_id=galleries.person_id
              GROUP BY persons.post_id";
	$persons=$newdb->get_results($query);
    //var_dump($persons);die;
	?>

	<div class="text-right">
		<label class="cd-label" for="directors"><a href="#" id="addDirector" class="btn btn-success btn-circle btn-xs "><i class="fa fa-plus"></i>Add New Director</a></label>
	</div>

	<div class="">
		<select id="selectpickerDirector" class="selectpickerDirector" name="video_director[]" multiple data-selected-text-format="count > 3" data-live-search="true">


			<?php foreach($persons as $key=>$person){?>
				<option data-image="<?php echo $person->image; ?>" value=" <?php echo $person->id; ?>"><?php echo $person->name; ?></option>
			<?php };

			?>

		</select>
	</div>

	<div id="dynamicDirector" class="text-center">

		<Label id="dynamicLabelDirector" style="font-size: large"></Label>

	</div>

	<div class="modal fade" id="directorModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Director</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="director_name" name="" placeholder="Enter Title">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default" id="directorSave">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function() {

			$(document).on('click','#addDirector',function(e) {
				e.preventDefault();
				$('#directorModal').modal('show');
			});

			$('#directorSave').on('click',function(e){
				e.preventDefault();
				var name=$('#director_name').val();
				//console.log(name);
				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_director',
						name : name,
						postId: <?php echo json_encode($post->ID);?>
					},
					success: function( id ) {

						$('#directorModal').modal('hide');
						ajaxDataHandler(name,id);

					}
				})
			})

			function ajaxDataHandler(name,id){

				var Div = $('#dynamicDirector');

				$('<p><label class="cd-label text-right " for="directors"></label>' +
					' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg directorData" data-id"'+id+'">' +name+ '</i>' +
					'</p>').appendTo(Div);

				var picker = document.getElementById("selectpickerDirector");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				option.selected = true;
				picker.add(option);
				$('.selectpickerDirector').selectpicker('refresh');

				updateOtherPickersFromDirector(name,id);
			}

			$('.selectpickerDirector').selectpicker({
				style: 'btn-success',
				size: 4
			});

			$('.selectpickerDirector').change(function () {

				var result = [];
				updateDirectorList();
				var Div = $('#dynamicDirector');
				var selectedText = $(this).val();

				for (var i = 0; i < selectedText.length; i++) {

					var val = selectedText[i];
					var txt = $(".selectpickerDirector option[value='"+val+"']").text();
					var image = $(".selectpickerDirector option[value='"+val+"']").data('image');

					result.push({
						'personId': val ,
						'personName': txt,
						'image':image
					});
				}

				var i=result.length;

				$('#dynamicLabelDirector').html('Selected Directors <i class="fa fa-chevron-circle-down"></i>')

				for(var j=0;j<i;j++)
				{
					$('<p><label class="cd-label text-right " for="directors"></label>' +
						' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg directorData" data-id="'+result[j].personId+'">' + result[j].personName + '</i>' +
						'<span style="margin-left:10px"><img id="person-image-director" class="img-circle" alt="" width="100px" height="100px" src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/'+result[j].image+'" /></span></p>').appendTo(Div);
				}

				$("img").error(function () {
					$(this).unbind("error").attr("src", "<?php bloginfo('template_url');?>/jQuery.filer/uploads/photo_not_available.png");
				});

			});

			function updateDirectorList(){
				$("#dynamicDirector p").remove();
			}

			$('#dynamicDirector').on('click','.directorData', function() {

				$(this).parent().remove();
				var value=$(this).data('id');

				$('.selectpickerDirector').find("option[value='"+value+"']").prop('selected', false);
				var values = $('.selectpickerDirector').val();

				$('.selectpickerDirector').selectpicker('val', values );

				if(values==null){
					$('#dynamicLabelDirector').html('No Directors Selected<i class="fa fa-users"></i>')
				}

			});

			function updateOtherPickersFromDirector(name,id){

				var picker = document.getElementById("selectpickerWriter");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerWriter').selectpicker('refresh');

				var picker = document.getElementById("selectpickerSinger");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerSinger').selectpicker('refresh');

				var picker = document.getElementById("selectpickerArtist");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerArtist').selectpicker('refresh');

				var picker = document.getElementById("selectpickerAuthor");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerAuthor').selectpicker('refresh');

				var picker = document.getElementById("selectpickerProducer");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerProducer').selectpicker('refresh');

			}

		});

	</script>

	<?php
}

add_action( 'wp_ajax_post_add_director', 'save_Director_On_The_Fly' );

function save_Director_On_The_Fly() {

	$postId=$_POST['postId']+1;
	$data=$_POST['name'];

	$entry_id = wp_insert_post(array (
		'post_type' => 'persons',
		'post_title' => $data,
		'post_status' => 'publish',
	));

    global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$newdb->query($newdb->prepare(" INSERT INTO persons (post_id,name,created_at)
                                       VALUES (%d,%s,%s)",
		array(
			$entry_id,
			$data,
			current_time('mysql'),
		)));
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $newdb->insert_id;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}


function video_author(){

	global $post;
	global   $newdb;

	$author=get_post_meta($post->ID, 'video_author',true);

	//var_dump($post->ID);
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT galleries.image,persons.id,persons.name FROM persons LEFT JOIN galleries ON persons.post_id=galleries.person_id
              GROUP BY persons.post_id";
	$persons=$newdb->get_results($query);

	?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<div class="text-right">
		<label class="cd-label" for="artists"><a href="#" id="addAuthor" class="btn btn-success btn-circle btn-xs "><i class="fa fa-plus"></i>Add New Author</a></label>
	</div>

	<div class="">
		<select id="selectpickerAuthor" class="selectpickerAuthor" name="video_author[]" multiple data-selected-text-format="count > 3" data-live-search="true">


			<?php foreach($persons as $key=>$person){?>
				<option data-image="<?php echo $person->image; ?>" value=" <?php echo $person->id; ?>"><?php echo $person->name; ?></option>
			<?php };

			?>

		</select>
	</div>

	<div id="dynamicAuthor" class="text-center">

		<Label id="dynamicLabelAuthor" style="font-size: large"></Label>

	</div>

	<div class="modal fade" id="authorModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Author</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="author_name" name="" placeholder="Enter Title">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default" id="authorSave">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function() {

			$(document).on('click','#addAuthor',function(e) {
				e.preventDefault();
				$('#authorModal').modal('show');
			});

			$('#authorSave').on('click',function(e){
				e.preventDefault();
				var name=$('#author_name').val();
				//console.log(name);
				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_author',
						name : name,
						postId: <?php echo json_encode($post->ID);?>
					},
					success: function( id ) {

						$('#authorModal').modal('hide');
						ajaxDataHandler(name,id);

					}
				})
			})

			function ajaxDataHandler(name,id){

				var Div = $('#dynamicAuthor');

				$('<p><label class="cd-label text-right " for="artists"></label>' +
					' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg authorData" data-id"'+id+'">' +name+ '</i>' +
					'</p>').appendTo(Div);

				var picker = document.getElementById("selectpickerAuthor");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				option.selected = true;
				picker.add(option);
				$('.selectpickerAuthor').selectpicker('refresh');

				updateOtherPickersFromAuthor(name,id);
			}

			$('.selectpickerAuthor').selectpicker({
				style: 'btn-info',
				size: 4
			});


			$('.selectpickerAuthor').change(function () {

				var result = [];
				updateAuthorList();
				var Div = $('#dynamicAuthor');
				var selectedText = $(this).val();

				for (var i = 0; i < selectedText.length; i++) {

					var val = selectedText[i];
					var txt = $(".selectpickerAuthor option[value='"+val+"']").text();
					var image = $(".selectpickerAuthor option[value='"+val+"']").data('image');

					result.push({
						'personId': val ,
						'personName': txt,
						'image':image
					});
				}

				var i=result.length;

				$('#dynamicLabelAuthor').html('Selected Authors <i class="fa fa-chevron-circle-down"></i>')

				for(var j=0;j<i;j++)
				{
					$('<p><label class="cd-label text-right " for="authors"></label>' +
						' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg authorData" data-id="'+result[j].personId+'">' + result[j].personName + '</i>' +
						'<span style="margin-left:10px"><img id="person-image-author" class="img-circle" alt="" width="100px" height="100px" src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/'+result[j].image+'" /></span></p>').appendTo(Div);
				}

				$("img").error(function () {
					$(this).unbind("error").attr("src", "<?php bloginfo('template_url');?>/jQuery.filer/uploads/photo_not_available.png");
				});

			});

			function updateAuthorList(){
				$("#dynamicAuthor p").remove();
			}

			$('#dynamicAuthor').on('click','.authorData', function() {

				$(this).parent().remove();
				var value=$(this).data('id');

				$('.selectpickerAuthor').find("option[value='"+value+"']").prop('selected', false);
				var values = $('.selectpickerAuthor').val();
				//console.log(values);

				$('.selectpickerAuthor').selectpicker('val', values );

				if(values==null){
					$('#dynamicLabelAuthor').html('No Authors Selected<i class="fa fa-users"></i>')
				}

			});

			function updateOtherPickersFromAuthor(name,id){

				var picker = document.getElementById("selectpickerWriter");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerWriter').selectpicker('refresh');

				var picker = document.getElementById("selectpickerSinger");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerSinger').selectpicker('refresh');

				var picker = document.getElementById("selectpickerArtist");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerArtist').selectpicker('refresh');

				var picker = document.getElementById("selectpickerDirector");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerDirector').selectpicker('refresh');

				var picker = document.getElementById("selectpickerProducer");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerProducer').selectpicker('refresh');

			}

		});

	</script>

	<?php

}

add_action( 'wp_ajax_post_add_author', 'save_Author_On_The_Fly' );

function save_Author_On_The_Fly() {

	$postId=$_POST['postId']+1;
	$data=$_POST['name'];

	$entry_id = wp_insert_post(array (
		'post_type' => 'persons',
		'post_title' => $data,
		'post_status' => 'publish',
	));

	global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$newdb->query($newdb->prepare(" INSERT INTO persons (post_id,name,created_at)
                                       VALUES (%d,%s,%s)",
		array(
			$entry_id,
			$data,
			current_time('mysql'),
		)));
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $newdb->insert_id;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}


function video_artist(){

	global $post;
	global   $newdb;

	$artist = get_post_meta($post->ID, 'video_artist',true);
	$artistsArray = explode(',', $artist);
	//var_dump($artistsArray);
	//var_dump($post->ID);
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT galleries.image,persons.id,persons.name FROM persons LEFT JOIN galleries ON persons.post_id=galleries.person_id
              GROUP BY persons.post_id";
	$persons=$newdb->get_results($query);


	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

	<div class="text-right">
		<label class="cd-label" for="artists"><a href="#" id="addArtist" class="btn btn-success btn-circle btn-xs "><i class="fa fa-plus"></i>Add New Artist</a></label>
	</div>

	<div class="">
	  <select id="selectpickerArtist" class="selectpickerArtist" name="video_artist[]" multiple data-selected-text-format="count > 3" data-live-search="true">


		  <?php foreach($persons as $key=>$person){?>
			  <option data-image="<?php echo $person->image; ?>" value=" <?php echo $person->id; ?>"><?php echo $person->name; ?></option>
		  <?php };

		  ?>

	  </select>
	</div>

	<div id="dynamicArtist" class="text-center">

		<Label id="dynamicLabelArtist" style="font-size: large"></Label>

	</div>

	<div class="modal fade" id="artistModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Artist</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name" name="" placeholder="Enter Title">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default" id="artistSave">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function() {

			$(document).on('click','#addArtist',function(e) {
				e.preventDefault();
				$('#artistModal').modal('show');
			});

			$('#artistSave').on('click',function(e){
				e.preventDefault();
				var name=$('#name').val();
				//console.log(name);
				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_artist',
						name : name,
						postId: <?php echo json_encode($post->ID);?>
					},
					success: function( id ) {
						//console.log(data);
						$('#artistModal').modal('hide');
						ajaxDataHandler(name,id);

					}
				})
			})

			function ajaxDataHandler(name,id){

				var Div = $('#dynamicArtist');

				$('<p><label class="cd-label text-right " for="artists"></label>' +
					' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg artistData" data-id="'+id+'">' +name+ '</i>' +
					'</p>').appendTo(Div);

				var picker = document.getElementById("selectpickerArtist");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				option.selected = true;
				picker.add(option);
				$('.selectpickerArtist').selectpicker('refresh');

				updateOtherPickersFromArtist(name,id);
			}

			$('.selectpickerArtist').selectpicker({
				style: 'btn-primary',
				size: 4
			});


			$('.selectpickerArtist').change(function () {

				var result = [];
				updateArtistList();
				var Div = $('#dynamicArtist');
				var selectedText = $(this).val();

				for (var i = 0; i < selectedText.length; i++) {

					var val = selectedText[i];
					var txt = $(".selectpickerArtist option[value='"+val+"']").text();
					var image = $(".selectpickerArtist option[value='"+val+"']").data('image');


					result.push({
						'personId': val ,
						'personName': txt,
						'image':image
					});
				}

				var i=result.length;

				$('#dynamicLabelArtist').html('Selected Artists <i class="fa fa-chevron-circle-down"></i>')

				for(var j=0;j<i;j++)
				{
					$('<p><label class="cd-label text-right " for="artists"></label>' +
						' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg artistData" data-id="'+result[j].personId+'">' + result[j].personName + '</i>' +
						'<span style="margin-left:10px"><img id="person-image-artist" class="img-circle" alt="" width="100px" height="100px" src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/'+result[j].image+'" /></span></p>').appendTo(Div);
				}

				$("img").error(function () {
					$(this).unbind("error").attr("src", "<?php bloginfo('template_url');?>/jQuery.filer/uploads/photo_not_available.png");
				});

			});

			function updateArtistList(){
				$("#dynamicArtist p").remove();
			}

			$('#dynamicArtist').on('click','.artistData', function() {

				$(this).parent().remove();
				var value=$(this).data('id');

				$('.selectpickerArtist').find("option[value='"+value+"']").prop('selected', false);
				var values = $('.selectpickerArtist').val();
				//console.log(values);

				$('.selectpickerArtist').selectpicker('val', values );

				if(values==null){
					$('#dynamicLabelArtist').html('No Artists Selected<i class="fa fa-users"></i>')
				}

			});

			function updateOtherPickersFromArtist(name,id){

				var picker = document.getElementById("selectpickerWriter");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerWriter').selectpicker('refresh');

				var picker = document.getElementById("selectpickerSinger");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerSinger').selectpicker('refresh');

				var picker = document.getElementById("selectpickerAuthor");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerAuthor').selectpicker('refresh');

				var picker = document.getElementById("selectpickerDirector");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerDirector').selectpicker('refresh');

				var picker = document.getElementById("selectpickerProducer");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerProducer').selectpicker('refresh');

			}

		});
	</script>

	<?php

}

add_action( 'wp_ajax_post_add_artist', 'save_Artist_On_The_Fly' );

function save_Artist_On_The_Fly() {

	$postId=$_POST['postId']+1;
	$data=$_POST['name'];

	$entry_id = wp_insert_post(array (
		'post_type' => 'persons',
		'post_title' => $data,
		'post_status' => 'publish',
	));
    global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$newdb->query($newdb->prepare(" INSERT INTO persons (post_id,name,created_at)
                                       VALUES (%d,%s,%s)",
		array(
			$entry_id,
			$data,
			current_time('mysql'),
		)));
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $newdb->insert_id;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

function video_writer(){

	global $post;
    global   $newdb;
	$writers = get_post_meta($post->ID, 'video_writer',true);
	$writersArray = explode(',', $writers);

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT galleries.image,persons.id,persons.name FROM persons LEFT JOIN galleries ON persons.post_id=galleries.person_id
              GROUP BY persons.post_id";
	$persons=$newdb->get_results($query);


	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

	<div class="text-right">
		<label class="cd-label" for="persons"><a href="#" id="addWriter" class="btn btn-success btn-circle btn-xs "><i class="fa fa-plus"></i>Add New Writer</a></label>
	</div>

	<div class="">
		<select id="selectpickerWriter" class="selectpickerWriter" name="video_writer[]" multiple data-selected-text-format="count > 3" data-live-search="true">


			<?php foreach($persons as $key=>$person){?>
				<option data-image="<?php echo $person->image; ?>" value=" <?php echo $person->id; ?>"><?php echo $person->name; ?></option>
			<?php };

			?>

		</select>
	</div>

	<div id="dynamicWriter" class="text-center">

		<Label id="dynamicLabelWriter" style="font-size: large"></Label>

	</div>

	<div class="modal fade" id="writerModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Writer</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="writer_name" name="" placeholder="Enter Title">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default" id="writerSave">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function() {

			$(document).on('click','#addWriter',function(e) {
				e.preventDefault();
				$('#writerModal').modal('show');
			});

			$('#writerSave').on('click',function(e){
				e.preventDefault();
				var name=$('#writer_name').val();
				//console.log(name);
				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_writer',
						name : name,
						postId: <?php echo json_encode($post->ID);?>
					},
					success: function( id ) {
						//console.log(data);
						$('#writerModal').modal('hide');
						ajaxDataHandler(name,id);

					}
				})
			})

			function ajaxDataHandler(name,id){

				var Div = $('#dynamicWriter');

				$('<p><label class="cd-label text-right " for="writers"></label>' +
					' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg writerData" data-id="'+id+'">' +name+ '</i>' +
					'</p>').appendTo(Div);

				var picker = document.getElementById("selectpickerWriter");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				option.selected = true;
				picker.add(option);
				$('.selectpickerWriter').selectpicker('refresh');

				updateOtherPickersFromWriter(name,id);
			}

			$('.selectpickerWriter').selectpicker({
				style: 'btn-success',
				size: 4
			});


			$('.selectpickerWriter').change(function () {

				var result = [];
				updateWriterList();
				var Div = $('#dynamicWriter');
				var selectedText = $(this).val();

				for (var i = 0; i < selectedText.length; i++) {

					var val = selectedText[i];
					var txt = $(".selectpickerWriter option[value='"+val+"']").text();
					var image= $(".selectpickerWriter option[value='"+val+"']").data('image');

					result.push({
						'personId': val ,
						'personName': txt,
						'image':image
					});
				}

				var i=result.length;

				$('#dynamicLabelWriter').html('Selected Writers <i class="fa fa-chevron-circle-down"></i>')

				for(var j=0;j<i;j++)
				{
					$('<p><label class="cd-label text-right " for="writers"></label>' +
						' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg writerData" data-id="'+result[j].personId+'">' + result[j].personName + '</i>' +
						'<span style="margin-left:10px"><img id="person-image-writer" class="img-circle" alt="" width="100px" height="100px" src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/'+result[j].image+'" /></span></p>').appendTo(Div);
				}

				$("img").error(function () {
					$(this).unbind("error").attr("src", "<?php bloginfo('template_url');?>/jQuery.filer/uploads/photo_not_available.png");
				});

			});

			function updateWriterList(){
				$("#dynamicWriter p").remove();
			}

			$('#dynamicWriter').on('click','.writerData', function() {

				$(this).parent().remove();
				var value=$(this).data('id');

				$('.selectpickerWriter').find("option[value='"+value+"']").prop('selected', false);
				var values = $('.selectpickerWriter').val();
				//console.log(values);

				$('.selectpickerWriter').selectpicker('val', values );

				if(values==null){
					$('#dynamicLabelWriter').html('No Writers Selected<i class="fa fa-users"></i>')
				}

			});

			function updateOtherPickersFromWriter(name,id){

				var picker = document.getElementById("selectpickerArtist");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerArtist').selectpicker('refresh');

				var picker = document.getElementById("selectpickerSinger");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerSinger').selectpicker('refresh');

				var picker = document.getElementById("selectpickerAuthor");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerAuthor').selectpicker('refresh');

				var picker = document.getElementById("selectpickerDirector");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerDirector').selectpicker('refresh');

				var picker = document.getElementById("selectpickerProducer");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerProducer').selectpicker('refresh');

			}

		});
	</script>

	<?php

}


add_action( 'wp_ajax_post_add_writer', 'save_writer_On_The_Fly' );

function save_writer_On_The_Fly() {

	$postId=$_POST['postId']+1;
	$data=$_POST['name'];

	$entry_id = wp_insert_post(array (
		'post_type' => 'persons',
		'post_title' => $data,
		'post_status' => 'publish',
	));

    global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$newdb->query($newdb->prepare(" INSERT INTO persons (post_id,name,created_at)
                                       VALUES (%d,%s,%s)",
		array(
			$entry_id,
			$data,
			current_time('mysql'),
		)));
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $newdb->insert_id;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}


function video_singer(){

	global $post;
	global   $newdb;

	$singers = get_post_meta($post->ID, 'video_singer',true);
	$artists = get_post_meta($post->ID, 'video_artist',true);
	$writers = get_post_meta($post->ID, 'video_writer',true);
	$authors = get_post_meta($post->ID, 'video_author',true);
	$serial = get_post_meta($post->ID, 'video_serial',true);
	$directors = get_post_meta($post->ID, 'video_director',true);
	$producers = get_post_meta($post->ID, 'video_producer',true);
	$productionHouse = get_post_meta($post->ID, 'video_production_house',true);
	$vimeoUrl = get_post_meta($post->ID, 'video_url',true);

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT galleries.image,persons.id,persons.name FROM persons LEFT JOIN galleries ON persons.post_id=galleries.person_id
              GROUP BY persons.post_id";
	$persons=$newdb->get_results($query);

	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

	<div class="text-right">
		<label class="cd-label" for="persons"><a href="#" id="addSinger" class="btn btn-success btn-circle btn-xs "><i class="fa fa-plus"></i>Add New Singer</a></label>
	</div>

	<div class="">
		<select id="selectpickerSinger" class="selectpickerSinger" name="video_singer[]" multiple data-selected-text-format="count > 3" data-live-search="true">


			<?php foreach($persons as $key=>$person){?>
				<option data-image="<?php echo $person->image; ?>" value=" <?php echo $person->id; ?>"><?php echo $person->name; ?></option>
			<?php };

			?>

		</select>
	</div>

	<div id="dynamicSinger" class="text-center">

		<Label id="dynamicLabelSinger" style="font-size: large"></Label>

	</div>

	<div class="modal fade" id="singerModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add Singer</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="singer_name" name="" placeholder="Enter Title">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default" id="singerSave">Save</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function() {

			$(document).on('click','#addSinger',function(e) {
				e.preventDefault();
				$('#singerModal').modal('show');
			});

			$('#singerSave').on('click',function(e){
				e.preventDefault();
				var name=$('#singer_name').val();
				//console.log(name);
				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_singer',
						name : name,
						postId: <?php echo json_encode($post->ID);?>
					},
					success: function( id ) {
						//console.log(id);
						$('#singerModal').modal('hide');
						ajaxDataHandler(id,name);

					}
				})
			})

			function ajaxDataHandler(id,name){

				var Div = $('#dynamicSinger');

				$('<p><label class="cd-label text-right " for="singers"></label>' +
					' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg singerData" data-id="'+id+'">' +name+ '</i>' +
					'</p>').appendTo(Div);

				var picker = document.getElementById("selectpickerSinger");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				option.selected = true;
				picker.add(option);
				$('.selectpickerSinger').selectpicker('refresh');

				updateOtherPickersFromSinger(name,id);
			}

			$('.selectpickerSinger').selectpicker({
				style: 'btn-warning',
				size: 4
			});


			$('.selectpickerSinger').change(function () {

				var result = [];
				updateSingerList();
				var Div = $('#dynamicSinger');
				var selectedText = $(this).val();

				for (var i = 0; i < selectedText.length; i++) {

					var val = selectedText[i];
					var txt = $(".selectpickerSinger option[value='"+val+"']").text();
                    var image= $(".selectpickerSinger option[value='"+val+"']").data('image');

					result.push({
						'personId': val ,
						'personName': txt,
						'image':image
					});
				}
				console.log(result);

				var i=result.length;
				//console.log(selectedText[0]);
				$('#dynamicLabelSinger').html('Selected Singers <i class="fa fa-chevron-circle-down"></i>')

				for(var j=0;j<i;j++)
				{
					$('<p><label class="cd-label text-right " for="singers"></label>' +
						' <i style="cursor: pointer; color: green" class="fa fa-user-times fa-lg singerData" data-id="'+result[j].personId+'">' + result[j].personName + '</i>' +
						'<span style="margin-left:10px"><img id="person-image" class="img-circle" width="100px" height="100px" src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/'+result[j].image+'" /></span></p>').appendTo(Div);
				}

				$("img").error(function () {
					$(this).unbind("error").attr("src", "<?php bloginfo('template_url');?>/jQuery.filer/uploads/photo_not_available.png");
				});

			});

			function updateSingerList(){
				$("#dynamicSinger p").remove();
			}

			$('#dynamicSinger').on('click','.singerData', function() {

				$(this).parent().remove();
				var value=$(this).data('id');

				$('.selectpickerSinger').find("option[value='"+value+"']").prop('selected', false);
				var values = $('.selectpickerSinger').val();
				//console.log(values);

				$('.selectpickerSinger').selectpicker('val', values );

				if(values==null){
					$('#dynamicLabelSinger').html('No Singers Selected<i class="fa fa-users"></i>')
				}

			});

			function updateOtherPickersFromSinger(name,id){

				var picker = document.getElementById("selectpickerArtist");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerArtist').selectpicker('refresh');

				var picker = document.getElementById("selectpickerWriter");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerWriter').selectpicker('refresh');

				var picker = document.getElementById("selectpickerAuthor");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerAuthor').selectpicker('refresh');

				var picker = document.getElementById("selectpickerDirector");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerDirector').selectpicker('refresh');

				var picker = document.getElementById("selectpickerProducer");
				var option = document.createElement("option");
				option.text = name;
				option.value = id;
				picker.add(option);
				$('.selectpickerProducer').selectpicker('refresh');

			}

		});

		$(document).ready(function(){

			var authorList = <?php echo json_encode($authors); ?>;
			var artistList = <?php echo json_encode($artists); ?>;
			var singerList = <?php echo json_encode($singers); ?>;
			var writerList = <?php echo json_encode($writers); ?>;
			var directorList = <?php echo json_encode($directors); ?>;
			var producerList = <?php echo json_encode($producers); ?>;
			var serial = <?php echo json_encode($serial); ?>;
			var productionHouse = <?php echo json_encode($productionHouse); ?>;
			var vimeoUrl = <?php echo json_encode($vimeoUrl); ?>;

			var arraySinger = singerList.split(',');
			var arrayWriter = writerList.split(',');
			var arrayAuthor = authorList.split(',');
			var arrayArtist = artistList.split(',');
			var arrayDirector = directorList.split(',');
			var arrayProducer = producerList.split(',');

			$('.selectpickerSinger').selectpicker('val',arraySinger);
			$('.selectpickerAuthor').selectpicker('val',arrayAuthor);
			$('.selectpickerArtist').selectpicker('val',arrayArtist);
			$('.selectpickerWriter').selectpicker('val',arrayWriter);
			$('.selectpickerDirector').selectpicker('val',arrayDirector);
			$('.selectpickerProducer').selectpicker('val',arrayProducer);
			$('.selectpickerSerial').selectpicker('val',serial);
			$('.selectpickerProductionHouse').selectpicker('val',productionHouse);
			$('.selectpickerVimeo').selectpicker('val',vimeoUrl);

		})

	</script>

	<?php

}

add_action( 'wp_ajax_post_add_singer', 'save_singer_On_The_Fly' );

function save_singer_On_The_Fly() {

	$postId=$_POST['postId']+1;
	$data=$_POST['name'];
	global   $newdb;

	$entry_id = wp_insert_post(array (
		'post_type' => 'persons',
		'post_title' => $data,
		'post_status' => 'publish',
	));

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$newdb->query($newdb->prepare(" INSERT INTO persons (post_id,name,created_at)
                                       VALUES (%d,%s,%s)",
		array(
			$entry_id,
			$data,
			current_time('mysql'),
		)));
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $newdb->insert_id;
		die();
	}
	else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

function poster_image() {

	global $post;
	global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM galleries WHERE video_id=$post->ID";
	$images=$newdb->get_results($query);

	?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/jQuery.filer/js/jquery.filer.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/jQuery.filer/css/jquery.filer.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/thirdbellImage.css">

	<input class="widefat" type="file" name="files[]" id="filer_input" multiple="multiple">
	<div id="imageAdd"></div>

    <?php
	if($images!=NULL){?>
		<div id="">
			<?php foreach($images as $image){?>
				<a class="updateImage" data-id="<?php echo $image->id ?>">
					<img class="customImage" src="<?php echo $image->image?>" height="150px" width="160px" onmouseover="this.src='<?php bloginfo('template_url');?>/jQuery.filer/uploads/close-128.png'" onmouseout="this.src='<?php bloginfo('template_url');?>/jQuery.filer/uploads/<?php echo $image->image?>'">
				</a>
			<?php } ?>
		</div>
	<?php }
	?>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function(){
			$('#filer_input').filer({
				limit: 1,
				maxSize: 5,
				extensions: ['jpg', 'jpeg', 'png', 'gif'],
				changeInput: true,
				showThumbs: false,
				onRemove: function test(removedFiles){
					//removeFunction(removedFiles);
				},
				uploadFile: {
					url: "<?php bloginfo('template_url'); ?>/jQuery.filer/php/upload.php", //URL to which the request is sent {String}
					data: null, //Data to be sent to the server {Object}
					type: 'POST', //The type of request {String}
					enctype: 'multipart/form-data', //Request enctype {String}
					beforeSend:null, //A pre-request callback function {Function}
					success: function sendlink(response) {

						var temp = JSON.parse(response);
						//console.log(temp.metas[0].name);

						processImage(temp.metas[0].name);

					},
					error: null, //A function to be called if the request fails {Function}
					statusCode: null, //An object of numeric HTTP codes {Object}
					onProgress: null, //A function called while uploading file with progress percentage {Function}
					onComplete: null, //A function called when all files were uploaded {Function}

				}
			});

			function processImage(image_name){

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_poster',
						name : image_name,
						post_id: <?php echo json_encode($post->ID);?>
					},
					success: function( image_id ) {
						//console.log(image_id);
						showPicture(image_name,image_id,<?php echo json_encode($post->ID);?>);
					}
				})
			}

			function showPicture(name,id,post_id){
                var src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/poster/"+post_id+'_'+name;
				//var src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/poster/"+name;
				imageDiv=$('<a class="removeImage" data-id="'+id+'"><span class="imageSpan">'
					+'<img src="'+src+'" width="160px" height="150px" title="Remove">'
				    +'</span></a>');
				$('#imageAdd').append(imageDiv);
			}

			$('#imageAdd').on('click','.removeImage', function() {

				$(this).remove();
				var imageID=$(this).data('id');

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_delete_poster',
						ID : imageID,
					},
					success: function( message ) {
						//console.log(message);
						alert(message);
					}
				})

			});

			$('.updateImage').on('click', function() {

				$(this).remove();
				var imageID=$(this).data('id');

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_delete_image',
						ID : imageID,
					},
					success: function( message ) {
						console.log(message);
						alert(message);
					}
				})

			});

		})
	</script>

    <?php
}

add_action( 'wp_ajax_post_add_poster', 'save_poster_image' );

function save_poster_image() {

   		$old_directory = get_template_directory().'/jQuery.filer/uploads';
        $new_directory = get_template_directory().'/jQuery.filer/uploads/poster';
        if (!is_dir($new_directory)) {
            mkdir( $new_directory, 0777, true );
        }
        $old_file_name = $_POST['name'];
        $new_file_name=$_POST['post_id'].'_'.$old_file_name;
        rename($old_directory.'/'.$old_file_name , $new_directory.'/'.$new_file_name );

	$image_name= get_site_url().'/wp-content/themes/thirdrdbell/jQuery.filer/uploads/poster/'.$_POST['post_id'].'_'.$_POST['name'];
	//$image_name=$_POST['name'];
	$post_id=$_POST['post_id'];
	global $newdb;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");
		$table_name = "galleries";

		$newdb->insert( $table_name, array(
					'video_id' => $post_id,
					'person_id'=>NULL,
					'image'=>$image_name,
					'created_at'=>current_time( 'mysql' ),
				)
			);

		echo $newdb->insert_id;
		die();
	}else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

add_action( 'wp_ajax_post_delete_poster', 'delete_poster_image' );

function delete_poster_image() {

	$image_id=$_POST['ID'];
	global   $newdb;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");

		$newdb->delete('galleries', array('id' => $image_id), array('%d'));

		echo "Image Removed!";
		die();
	}else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}


function save_videos_meta($post_id, $post) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	$videoMeta=array(
		'video_type'=>$_POST['video_type'],
		'content_type'=>$_POST['content_type'],
		'video_serial'=>$_POST['video_serial'],
		'video_url'=>$_POST['video_url'],
		'video_author'=>$_POST['video_author'],
		'video_artist'=>$_POST['video_artist'],
		'video_writer'=>$_POST['video_writer'],
		'video_singer'=>$_POST['video_singer'],
		'video_cost'=>$_POST['video_cost'],
		'video_point'=>$_POST['video_point'],
		'video_duration'=>$_POST['video_duration'],
		'video_producer'=>$_POST['video_producer'],
		'video_production_house'=>$_POST['video_production_house'],
		'video_director'=>$_POST['video_director'],
		'video_territory'=>$_POST['video_territory'],

	);

	foreach ($videoMeta as $key => $value) { // Cycle through the $events_meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}

}

add_action('save_post', 'save_videos_meta', 1, 2); // saving the meta posts


function create_person_post_type() {

	$labels = array(
		'name' =>'Star Profile',
		'singular_name' =>'Star Profile',
		'add_new' =>'Add Profile',
		'all items' =>'All Profiles',
		'add_new_item' =>'Add Profile',
		'edit' =>'Edit Person',
		'view' =>'View Person',
	);

	$args=array(
		'labels'=>$labels,
		'public'=>true,
		'rewrite'=>true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'menu_position' => 8,
		'menu_icon'   => 'dashicons-groups',
		'register_meta_box_cb' => 'add_person_metaboxes'
	);

	register_post_type('persons',$args);

}
add_action('init','create_person_post_type' );

add_action( 'add_meta_boxes', 'add_person_metaboxes');

function add_person_metaboxes() {

	add_meta_box(
		'person_born', //id
		'Date of Birth', //title
		'person_born', //callbacks
		'persons', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'person_die', //id
		'Date of Death', //title
		'person_die', //callbacks
		'persons', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'person_gender', //id
		'Gender', //title
		'person_gender', //callbacks
		'persons', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'person_image', //id
		'Person Image', //title
		'person_image', //callbacks
		'persons', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);

}


function person_born() {

	global $post;

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the location data if its already been entered
	$person_born = get_post_meta($post->ID, 'person_born', true);

	// Echo out the field
	echo '<input class="widefat  datepicker" id="picker3"  type="text" name="person_born" value="' . $person_born  . '" placeholder="yyyy-mm-dd" data-date-format="yyyy-mm-dd" >';

	?>

	<script>
		var $ =jQuery.noConflict();
		jQuery(document).ready(function() {

			jQuery('#picker3').datepicker({
				dateFormat : 'yy-mm-dd',
				changeYear: true,
				yearRange: "-200:+0",
			});
		});
	</script>
	<?php

}

function person_die() {

	global $post;

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// Get the location data if its already been entered
	$person_die = get_post_meta($post->ID, 'person_die', true);

	// Echo out the field
	echo '<input type="text" id="picker4" name="person_die" value="' . $person_die  . '" class="widefat" placeholder="yyyy-mm-dd" />';

	?>

	<script>
		var $ =jQuery.noConflict();
		jQuery(document).ready(function() {
			jQuery('#picker4').datepicker({
				dateFormat : 'yy-mm-dd',
				changeYear: true,
				yearRange: "-200:+0",
			});
		});
	</script>

	<?php
}

function person_gender() {

	global $post;
	$gender = get_post_meta($post->ID, 'person_gender',true);

	?>

	<select class="widefat" name="person_gender">
		<option value="<?php echo $gender ?>" selected><?php echo $gender?></option>
		<option value="Male">Male</option>
		<option value="Female">Female</option>
	</select>

	<?php
}

function person_image(){

	global $post;
	global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM galleries WHERE person_id=$post->ID";
	$images=$newdb->get_results($query);

	?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/jQuery.filer/js/jquery.filer.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/jQuery.filer/css/jquery.filer.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/thirdbellImage.css">

	<input class="widefat" type="file" name="files[]" id="filer_input" multiple="multiple">
	<div id="imageAdd"></div>

	<?php
	if($images!=NULL){?>
		<div id="">
			<?php foreach($images as $image){?>
				<a class="updateImage" data-id="<?php echo $image->id ?>">
					<img class="customImage" src="<?php echo $image->image?>" height="150px" width="160px" onmouseover="this.src='<?php bloginfo('template_url');?>/jQuery.filer/uploads/close-128.png'" onmouseout="this.src='<?php bloginfo('template_url');?>/jQuery.filer/uploads/<?php echo $image->image?>'">
				</a>
			<?php } ?>
		</div>
	<?php }
	?>

	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function(){
			$('#filer_input').filer({
				limit: 5,
				maxSize: 5,
				extensions: ['jpg', 'jpeg', 'png', 'gif'],
				changeInput: true,
				showThumbs: false,
				onRemove: function test(removedFiles){
					//removeFunction(removedFiles);
				},
				uploadFile: {
					url: "<?php bloginfo('template_url'); ?>/jQuery.filer/php/upload.php", //URL to which the request is sent {String}
					data: null, //Data to be sent to the server {Object}
					type: 'POST', //The type of request {String}
					enctype: 'multipart/form-data', //Request enctype {String}
					beforeSend:null, //A pre-request callback function {Function}
					success: function sendlink(response) {

						var temp = JSON.parse(response);
						//console.log(temp.metas[0].name);

						processImage(temp.metas[0].name);

					},
					error: null, //A function to be called if the request fails {Function}
					statusCode: null, //An object of numeric HTTP codes {Object}
					onProgress: null, //A function called while uploading file with progress percentage {Function}
					onComplete: null, //A function called when all files were uploaded {Function}

				}
			});

			function processImage(image_name){

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_image',
						name : image_name,
						post_id: <?php echo json_encode($post->ID);?>
					},
					success: function( image_id ) {
						//console.log(image_id);
						showPicture(image_name,image_id,<?php echo json_encode($post->ID);?>);
					}
				})
			}

			function showPicture(name,id,post_id){

				var src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/person/"+post_id+'_'+name;
				imageDiv=$('<a class="removeImage" data-id="'+id+'"><span class="imageSpan">'
					+'<img src="'+src+'" width="160px" height="150px" title="Remove">'
				    +'</span></a>');
				$('#imageAdd').append(imageDiv);
			}

			$('#imageAdd').on('click','.removeImage', function() {

				$(this).remove();
				var imageID=$(this).data('id');

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_delete_image',
						ID : imageID,
					},
					success: function( message ) {
						//console.log(message);
						alert(message);
					}
				})

			});

			$('.updateImage').on('click', function() {

				$(this).remove();
				var imageID=$(this).data('id');

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_delete_image',
						ID : imageID,
					},
					success: function( message ) {
						console.log(message);
						alert(message);
					}
				})

			});
		})
	</script>

	<?php
}

add_action( 'wp_ajax_post_add_image', 'save_person_image' );

function save_person_image() {

        $old_directory = get_template_directory().'/jQuery.filer/uploads';
        $new_directory = get_template_directory().'/jQuery.filer/uploads/person';
        if (!is_dir($new_directory)) {
            mkdir( $new_directory, 0777, true );
        }
        $old_file_name = $_POST['name'];
        $new_file_name=$_POST['post_id'].'_'.$old_file_name;
        rename($old_directory.'/'.$old_file_name , $new_directory.'/'.$new_file_name );

	$image_name= get_site_url().'/wp-content/themes/thirdrdbell/jQuery.filer/uploads/person/'.$_POST['post_id'].'_'.$_POST['name'];
	//$image_name=$_POST['name'];
	$post_id=$_POST['post_id'];
	global $newdb;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");
		$table_name = "galleries";

		$newdb->insert( $table_name, array(
					'video_id' => NULL,
					'person_id'=>$post_id,
					'image'=>$image_name,
					'created_at'=>current_time( 'mysql' ),
				)
			);

		echo $newdb->insert_id;
		die();
	}else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

add_action( 'wp_ajax_post_delete_image', 'delete_person_image' );

function delete_person_image() {

	$image_id=$_POST['ID'];
	global   $newdb;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");

		$newdb->delete('galleries', array('id' => $image_id), array('%d'));

		echo "Image Removed!";
		die();
	}else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');


function admin_scripts()
{
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
}
function admin_styles()
{
	wp_enqueue_style('thickbox');
}

add_action('admin_print_scripts', 'admin_scripts');
add_action('admin_print_styles', 'admin_styles');


function save_persons_meta($post_id, $post) {

	if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	$personMeta=array(
		'person_born'=>$_POST['person_born'],
		'person_die'=>$_POST['person_die'],
		'person_gender'=>$_POST['person_gender'],
		'person_image'=>$_POST['person_image'],
	);

	foreach ($personMeta as $key => $value) { // Cycle through the $events_meta array!
		if( $post->post_type == 'revision' ) return; // Don't store custom data twice
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);

		}
		if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
	}

    global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "persons";
	$query = "SELECT post_id FROM persons WHERE post_id=$post->ID";
	$postID=$newdb->get_var($query);

    if($postID!=$post->ID){
		$newdb->insert( $table_name, array(
				'name' => $_POST['post_title'],
				'description'=> $_POST['content'],
				'post_id'=>$post->ID,
				'birth_date'=>$_POST['person_born'],
				'death_date'=>$_POST['person_die'],
				'gender'=>$_POST['person_gender'],
				'created_at'=>current_time( 'mysql' ),
			)
		);

	}else{
		$newdb->update(
			$table_name,
			array(
				'name' => $_POST['post_title'],
				'description'=> $_POST['content'],
				'post_id'=>$post->ID,
				'birth_date'=>$_POST['person_born'],
				'death_date'=>$_POST['person_die'],
				'gender'=>$_POST['person_gender'],
				'updated_at'=>current_time( 'mysql' ),
			),
			array( 'post_id' => $post->ID ),
			array('%s','%s','%d','%s','%s','%s','%s'),
			array( '%d' )
		);

	}

}

add_action('save_post_persons', 'save_persons_meta', 1, 2); // saving the meta posts

add_action('delete_post', 'person_delete_function');

function person_delete_function(){

	if(!did_action('trash_post')){

		global $post;
		global   $newdb;

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");
		$table_name = "persons";
		$newdb->delete( $table_name, array( 'post_id' => $post->ID ) );
	}
}

add_action('delete_post', 'video_delete_function');

function video_delete_function(){

	if(!did_action('trash_post')){

		global $post;
		global   $newdb;

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");
		$table_name = "videos";
		$newdb->delete( $table_name, array( 'post_id' => $post->ID ) );
	}
}

add_action('delete_post', 'productionHouse_delete_function');

function productionHouse_delete_function(){

	if(!did_action('trash_post')){

		global $post;
		global   $newdb;

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");
		$table_name = "production_houses";
		$newdb->delete( $table_name, array( 'post_id' => $post->ID ) );
	}
}

add_action('delete_post', 'serial_delete_function');

function serial_delete_function(){

	if(!did_action('trash_post')){

		global $post;
		global   $newdb;

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");
		$table_name = "serials";
		$newdb->delete( $table_name, array( 'post_id' => $post->ID ) );
	}
}

function create_serial_post_type() {

	$labels = array(
		'name' =>'Serial',
		'singular_name' =>'serial',
		'add_new' =>'add serial',
		'all items' =>'All serial',
		'add_new_item' =>'Add serial',
		'edit' =>'Edit serial',
		'view' =>'View serial',
	);

	$args=array(
		'labels'=>$labels,
		'public'=>true,
		'rewrite'=>true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'menu_position' => 8,
		'menu_icon'   => 'dashicons-format-video',
	);

	register_post_type('serials',$args);

}
add_action('init','create_serial_post_type' );


function save_serial_data() {

	global $post;
	global   $newdb;

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "serials";
	$query = "SELECT post_id FROM serials WHERE post_id=$post->ID";
	$postID=$newdb->get_var($query);

	if($postID!=$post->ID) {
		$newdb->query($newdb->prepare(" INSERT INTO serials (post_id,name,description)
                                       VALUES (%d,%s,%s)",
			array(
				$post->ID,
				$_POST['post_title'],
				$_POST['content'],
			)));
	}else{
		$newdb->update(
			$table_name,
			array(
				'post_id'=>$post->ID,
				'name' => $_POST['post_title'],
				'description'=> $_POST['content'],
			),
			array( 'post_id' => $post->ID ),
			array('%d','%s','%s'),
			array( '%d' )
		);
	}
}

add_action('save_post_serials', 'save_serial_data');


function no_wp_logo_admin_bar_remove() {

	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', 'no_wp_logo_admin_bar_remove', 0);

add_action( 'save_post_videos', 'video_data_save',1,2 );

function video_data_save()
{

	$video_url = $_POST['video_url'];

	preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $video_url, $output_array);
	$vimeoVideoId=$output_array[5];


	require(get_template_directory()."/vendor/vimeo/vimeo-api/autoload.php");
	$client_identifier = 'd69e92c8f373e65c6373451df4ba956cf96d2ba7';
	$client_secrets = 'DidG0ITAV+9E3DT3TtGeR5LDIecUlhRna69mz5mJzipXeH5yn4f95hg0k74OwQUDLIHxW9FXKUKoT7FYOtETyg+P2IZ6DTZLPcSCW1CwSI58qvkVeF+N/5FhIQf/iciS';
	$lib = new \Vimeo\Vimeo($client_identifier, $client_secrets);

	$lib->setToken('06288c519c968c834ea185b0ba3e5658');
	$response = $lib->request('/me/videos/'.$vimeoVideoId.'', 'GET');

	$indexSize=sizeof($response['body']['pictures']['sizes']);
	$encodedQuality=json_encode($response['body']['files']);
	$duration=gmdate("H:i:s", $response['body']['duration']);
	$vimeoThumbnail=$response['body']['pictures']['sizes'][$indexSize-1]['link'];

	global $post;
	global   $newdb;

	//$newdb = new wpdb("root", "", "3rdbell_database", "localhost");
	$table_name = "videos";
	$query = "SELECT post_id FROM videos WHERE post_id=$post->ID";
	$postID = $newdb->get_var($query);


	$tags = wp_get_post_tags($post->ID);
	$encodedTags=json_encode($tags);
	$wpThumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	//var_dump($feat_image);die;
    if($wpThumbnail==NULL){
		$thumbnail=$vimeoThumbnail;
	}else{
		$thumbnail=$wpThumbnail;
	}

	if ($postID != $post->ID) {
		$newdb->query($newdb->prepare(" INSERT INTO videos (post_id,type,serial_id,title,description,territory,duration,revenue_share,production_house_id,link,point,tag,quality,created_at,thumbnail)
                                       VALUES (%d,%s,%d,%s,%s,%s,%s,%f,%d,%s,%d,%s,%s,%s,%s)",
			array(
				$post->ID,
				$_POST['video_type'],
				$_POST['video_serial'],
				$_POST['post_title'],
				$_POST['content'],
				$_POST['video_territory'],
				$duration,
				$_POST['video_cost'],
				$_POST['video_production_house'],
				$_POST['video_url'],
				$_POST['video_point'],
				$encodedTags,
				$encodedQuality,
				current_time('mysql'),
				$thumbnail,
			)));


		$catArray = $_POST['post_category'];
		$artistArray = $_POST['video_artist'];
		$authorArray = $_POST['video_author'];
		$writerArray = $_POST['video_writer'];
		$singerArray = $_POST['video_singer'];
		$directorArray = $_POST['video_director'];
		$producerArray = $_POST['video_producer'];

		$video_id = $newdb->insert_id;

		foreach ($catArray as $key => $category) {
			if ($category != 0 )
			 {
				$query = "SELECT parent_id FROM categories WHERE category_id=$category";
				$parentId = $newdb->get_var($query);
				if($parentId!=0){
				$newdb->query($newdb->prepare(" INSERT INTO category_video (video_id,category_id,created_at)VALUES (%d,%d,%s)",
					array(
						$video_id,
						$parentId,
						current_time('mysql'),
					)));
				}

				$newdb->query($newdb->prepare(" INSERT INTO category_video (video_id,category_id,created_at)VALUES (%d,%d,%s)",
					array(
						$video_id,
						$category,
						current_time('mysql'),
					)));
			}
		}

		foreach ($artistArray as $key => $artist) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at)VALUES (%d,%d,%s,%s)",
				array(
					$video_id,
					$artist,
					'artist',
					current_time('mysql'),
				)));
		}

		foreach ($authorArray as $key => $author) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at)VALUES (%d,%d,%s,%s)",
				array(
					$video_id,
					$artist,
					'author',
					current_time('mysql'),
				)));
		}

		foreach ($writerArray as $key => $writer) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at)VALUES (%d,%d,%s,%s)",
				array(
					$video_id,
					$writer,
					'writer',
					current_time('mysql'),
				)));
		}

		foreach ($singerArray as $key => $singer) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at)VALUES (%d,%d,%s,%s)",
				array(
					$video_id,
					$singer,
					'singer',
					current_time('mysql'),
				)));
		}

		foreach ($directorArray as $key => $director) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at)VALUES (%d,%d,%s,%s)",
				array(
					$video_id,
					$director,
					'director',
					current_time('mysql'),
				)));
		}

		foreach ($producerArray as $key => $producer) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at)VALUES (%d,%d,%s,%s)",
				array(
					$video_id,
					$producer,
					'producer',
					current_time('mysql'),
				)));
		}

		//elasticsearch api call
		$url="https://localhost:8000/api/wp-admin/elastic-search-data/".$video_id ;
		file_get_contents($url);

	} else {
	    global   $newdb;
		//$newdb = new wpdb("root", "", "3rdbell_database", "localhost");
		$table_name = "videos";
		$query = "SELECT id FROM videos WHERE post_id=$post->ID";
		$video_id = $newdb->get_var($query);
		$query = "SELECT created_at FROM videos WHERE post_id=$post->ID";
		$created_at = $newdb->get_var($query);

		$newdb->update(
			$table_name,
			array(
				'post_id' => $post->ID,
				'type' => $_POST['video_type'],
				'serial_id' => $_POST['video_serial'],
				'title' => $_POST['post_title'],
				'description' => $_POST['content'],
				'territory' => $_POST['video_territory'],
				'duration' => $duration,
				'revenue_share' => $_POST['video_cost'],
				'thumbnail' => $thumbnail,
				'production_house_id' => $_POST['video_production_house'],
				'link' => $_POST['video_url'],
				'point' => $_POST['video_point'],
				'tag' => $encodedTags,
				'quality'=> $encodedQuality,
				'updated_at' => current_time('mysql'),
			),
			array('post_id' => $post->ID),
			array('%d', '%s', '%d', '%s', '%s', '%s', '%s', '%f','%s', '%d', '%s', '%d', '%s','%s','%s'),
			array('%d')
		);

		$catArray = $_POST['post_category'];
		$artistArray = $_POST['video_artist'];
		$authorArray = $_POST['video_author'];
		$writerArray = $_POST['video_writer'];
		$singerArray = $_POST['video_singer'];
		$directorArray = $_POST['video_director'];
		$producerArray = $_POST['video_producer'];

		$newdb->delete('category_video', array('video_id' => $video_id), array('%d'));

		foreach ($catArray as $key => $category) {
			if ($category != 0 )
			 {
				$query = "SELECT parent_id FROM categories WHERE category_id=$category";
				$parentId = $newdb->get_var($query);
				if($parentId!=0){
				$newdb->query($newdb->prepare(" INSERT INTO category_video (video_id,category_id,created_at)VALUES (%d,%d,%s)",
					array(
						$video_id,
						$parentId,
						current_time('mysql'),
					)));
				}

				$newdb->query($newdb->prepare(" INSERT INTO category_video (video_id,category_id,created_at)VALUES (%d,%d,%s)",
					array(
						$video_id,
						$category,
						current_time('mysql'),
					)));
			}
		}

		$newdb->delete('person_video', array('video_id' => $video_id), array('%d'));

		foreach ($artistArray as $key => $artist) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at,updated_at)VALUES (%d,%d,%s,%s,%s)",
				array(
					$video_id,
					$artist,
					'artist',
					$created_at,
					current_time('mysql'),
				)));
		}

		foreach ($authorArray as $key => $author) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at,updated_at)VALUES (%d,%d,%s,%s,%s)",
				array(
					$video_id,
					$author,
					'author',
					$created_at,
					current_time('mysql'),
				)));
		}

		foreach ($writerArray as $key => $writer) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at,updated_at)VALUES (%d,%d,%s,%s,%s)",
				array(
					$video_id,
					$writer,
					'writer',
					$created_at,
					current_time('mysql'),
				)));
		}

		foreach ($singerArray as $key => $singer) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at,updated_at)VALUES (%d,%d,%s,%s,%s)",
				array(
					$video_id,
					$singer,
					'singer',
					$created_at,
					current_time('mysql'),
				)));
		}

		foreach ($directorArray as $key => $director) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at,updated_at)VALUES (%d,%d,%s,%s,%s)",
				array(
					$video_id,
					$director,
					'director',
					$created_at,
					current_time('mysql'),
				)));

		}

		foreach ($producerArray as $key => $producer) {
			$newdb->query($newdb->prepare(" INSERT INTO person_video (video_id,person_id,role,created_at,updated_at)VALUES (%d,%d,%s,%s,%s)",
				array(
					$video_id,
					$producer,
					'producer',
					$created_at,
					current_time('mysql'),
				)));

		}

		$url="https://localhost:8000/api/wp-admin/elastic-search-data/".$video_id ;
		file_get_contents($url);
	}
}


function create_production_house_post_type() {

	$labels = array(
		'name' =>'Production House',
		'singular_name' =>'Production House',
		'add_new' =>'add Production House',
		'all items' =>'All Production House',
		'add_new_item' =>'Add Production House',
		'edit' =>'Edit Production House',
		'view' =>'View Production House',
	);

	$args=array(
		'labels'=>$labels,
		'public'=>true,
		'rewrite'=>true,
		'supports' => array( 'title', 'editor','thumbnail' ),
		'menu_position' => 8,
		'menu_icon'   => 'dashicons-admin-multisite',
	);

	register_post_type('Production_House',$args);

}
add_action('init','create_production_house_post_type' );


function save_production_house_data() {

	global $post;
	global   $newdb;

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	$wpThumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "production_houses";
	$query = "SELECT post_id FROM production_houses WHERE post_id=$post->ID";
	$postID=$newdb->get_var($query);

	if($postID!=$post->ID) {
		$newdb->query($newdb->prepare(" INSERT INTO production_houses (post_id,name,description,image,created_at)
                                       VALUES (%d,%s,%s,%s,%s)",
			array(
				$post->ID,
				$_POST['post_title'],
				$_POST['content'],
				$wpThumbnail,
				current_time('mysql'),
			)));
	}else{
		$newdb->update(
			$table_name,
			array(
				'post_id'=>$post->ID,
				'name' => $_POST['post_title'],
				'description'=> $_POST['content'],
				'image'=>$wpThumbnail,
				'updated_at'=>current_time( 'mysql' ),
			),
			array( 'post_id' => $post->ID ),
			array('%d','%s','%s','%s','%s'),
			array( '%d' )
		);
	}
}

add_action('save_post_production_house', 'save_production_house_data');



function create_trending_video_post_type() {

	$labels = array(
		'name' =>'Trending Video',
		'singular_name' =>'Trending Vodeo',
		'add_new' =>'add Trending Video',
		'all items' =>'All Trending Videos',
		'add_new_item' =>'Add Trending Video',
		'edit' =>'Edit Trending Video',
		'view' =>'View Trending Video',
	);

	$args=array(
		'labels'=>$labels,
		'public'=>true,
		'rewrite'=>true,
		'supports' => array( 'title'),
		'menu_position' => 8,
		'menu_icon'   => 'dashicons-visibility',
		'register_meta_box_cb' => 'add_trending_videos_metaboxes'
	);

	register_post_type('trending_video',$args);

}
add_action('init','create_trending_video_post_type' );

add_action( 'add_meta_boxes', 'add_trending_videos_metaboxes');

function add_trending_videos_metaboxes() {

	add_meta_box(
		'trending_video', //id
		'trending video', //title
		'trending_video', //callbacks
		'trending_video', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);

}

function trending_video() {

    global $post;
    global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM videos";
	$videos=$newdb->get_results($query);

	$query = "SELECT video_id FROM trending_videos WHERE post_id=$post->ID";
	$trending_video=$newdb->get_var($query);
	//var_dump($trending_video);die;
	$query = "SELECT status FROM trending_videos WHERE post_id=$post->ID";
	$status=$newdb->get_var($query);
	?>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">



	<select id="selectpickerTrending" class="selectpickerTrending" name="trending_video_id" data-live-search="true" >

		<?php foreach($videos as $key=>$video){?>
			<option data-content="<img src='<?php echo $video->thumbnail; ?>' width='100px' height='100px'> <?php echo $video->title; ?>" value=<?php echo $video->id; ?> data-width="75%">
				<?php echo $video->title; ?></option>
		<?php };

		?>

	</select>


	<input type="radio" name="video_status" value="Active" <?php if ("Active" == $status ) echo 'checked' ; ?>> Active
	<input type="radio" name="video_status" value="Inactive" <?php if ("Inactive" == $status ) echo 'checked' ; ?>> Inactive

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


    <script type="text/javascript">

		var $ =jQuery.noConflict();

		$(document).ready(function(){

            var trendingVideo = <?php echo json_encode($trending_video); ?>;
			$('.selectpickerTrending').selectpicker({
				//val: 2,
				style: 'btn-warning',
				size: 8
			});
		})

        $(document).ready(function(){

			var trendingVideo = <?php echo json_encode($trending_video); ?>;
			//console.log(trendingVideo);

            $('.selectpickerTrending').selectpicker('val',trendingVideo);

		})
	</script>

	<?php

}

function save_trending_video_data() {

	global $post;
	global   $newdb;

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;


	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "trending_videos";
	$query = "SELECT post_id FROM trending_videos WHERE post_id=$post->ID";
	$postID=$newdb->get_var($query);

	if($postID!=$post->ID) {
		$newdb->query($newdb->prepare(" INSERT INTO trending_videos (post_id,video_id,status)
                                       VALUES (%d,%d,%s)",
			array(
				$post->ID,
				$_POST['trending_video_id'],
				$_POST['video_status'],
			)));
	}else{
		$newdb->update(
			$table_name,
			array(
				'post_id'=>$post->ID,
				'video_id' => $_POST['trending_video_id'],
				'status'=> $_POST['video_status'],
			),
			array( 'post_id' => $post->ID ),
			array('%d','%d','%s'),
			array( '%d' )
		);
	}
}

add_action('save_post_trending_video', 'save_trending_video_data');

// Recomended videos post type

function create_recommended_video_post_type() {

	$labels = array(
		'name' =>'Recommended Video',
		'singular_name' =>'Recommended Video',
		'add_new' =>'add Recommended Video',
		'all items' =>'All Recommended Videos',
		'add_new_item' =>'Add Recommended Video',
		'edit' =>'Edit Recommended Video',
		'view' =>'View Recommended Video',
	);

	$args=array(
		'labels'=>$labels,
		'public'=>true,
		'rewrite'=>true,
		'supports' => array( 'title'),
		'menu_position' => 8,
		'menu_icon'   => 'dashicons-megaphone',
		'register_meta_box_cb' => 'add_recommended_videos_metaboxes'
	);

	register_post_type('recommended_video',$args);

}
add_action('init','create_recommended_video_post_type' );

add_action( 'add_meta_boxes', 'add_recommended_videos_metaboxes');

function add_recommended_videos_metaboxes() {

	add_meta_box(
		'recommended_video', //id
		'Recommended Video', //title
		'recommended_video', //callbacks
		'recommended_video', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);

}

function recommended_video() {

	global $post;
	global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM videos";
	$videos=$newdb->get_results($query);

	$query = "SELECT video_id FROM recommended_videos WHERE post_id=$post->ID";
	$recommended_video=$newdb->get_var($query);
	$query = "SELECT active FROM recommended_videos WHERE post_id=$post->ID";
	$active=$newdb->get_var($query);
	?>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">



	<select id="selectpickerRecommended" class="selectpickerRecommended" name="recommended_video_id" data-live-search="true" >

		<?php foreach($videos as $key=>$video){?>
			<option data-content="<img src='<?php echo $video->thumbnail; ?>' width='100px' height='100px'> <?php echo $video->title; ?>" value=<?php echo $video->id; ?> data-width="75%">
				<?php echo $video->title; ?></option>
		<?php };

		?>

	</select>


	<input type="radio" name="video_active" value="1" <?php if ("1" == $active ) echo 'checked' ; ?>> Active
	<input type="radio" name="video_active" value="0" <?php if ("0" == $active ) echo 'checked' ; ?>> Inactive

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


    <script type="text/javascript">

		var $ =jQuery.noConflict();
		$(document).ready(function(){

			$('.selectpickerRecommended').selectpicker({
				style: 'btn-warning',
				size: 8
			});
		})


        $(document).ready(function(){

			var recommendedVideo = <?php echo json_encode($recommended_video); ?>;

            $('.selectpickerRecommended').selectpicker('val',recommendedVideo);

		})
	</script>

	<?php

}

function save_recommended_video_data() {

	global $post;
	global   $newdb;

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;


	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "recommended_videos";
	$query = "SELECT post_id FROM recommended_videos WHERE post_id=$post->ID";
	$postID=$newdb->get_var($query);

	if($postID!=$post->ID) {
		$newdb->query($newdb->prepare(" INSERT INTO recommended_videos (post_id,video_id,active)
                                       VALUES (%d,%d,%s)",
			array(
				$post->ID,
				$_POST['recommended_video_id'],
				$_POST['video_active'],
			)));
	}else{
		$newdb->update(
			$table_name,
			array(
				'post_id'=>$post->ID,
				'video_id' => $_POST['recommended_video_id'],
				'active'=> $_POST['video_active'],
			),
			array( 'post_id' => $post->ID ),
			array('%d','%d','%s'),
			array( '%d' )
		);
	}
}

add_action('save_post_recommended_video', 'save_recommended_video_data');

//popular music post type

function create_popular_music_post_type() {

	$labels = array(
		'name' =>'Popular Music',
		'singular_name' =>'Popular Music',
		'add_new' =>'add Popular Music',
		'all items' =>'All Popular Musics',
		'add_new_item' =>'Add Popular Music',
		'edit' =>'Edit Popular Music',
		'view' =>'View Popular Music',
	);

	$args=array(
		'labels'=>$labels,
		'public'=>true,
		'rewrite'=>true,
		'supports' => array( 'title'),
		'menu_position' => 8,
		'menu_icon'   => 'dashicons-controls-volumeon',
		'register_meta_box_cb' => 'add_popular_music_metaboxes'
	);

	register_post_type('popular_music',$args);

}
add_action('init','create_popular_music_post_type' );

add_action( 'add_meta_boxes', 'add_popular_music_metaboxes');

function add_popular_music_metaboxes() {

	add_meta_box(
		'popular_music', //id
		'Popular Music', //title
		'popular_music', //callbacks
		'popular_music', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);

}

function popular_music() {

	global $post;
	global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM videos";
	$videos=$newdb->get_results($query);

	$query = "SELECT video_id FROM popular_music WHERE post_id=$post->ID";
	$popular_music=$newdb->get_var($query);
	$query = "SELECT active FROM popular_music WHERE post_id=$post->ID";
	$active=$newdb->get_var($query);
	?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">



	<select id="selectpickerPopularMusic" class="selectpickerPopularMusic" name="popular_music_id" data-live-search="true" >

		<?php foreach($videos as $key=>$video){?>
			<option data-content="<img src='<?php echo $video->thumbnail; ?>' width='100px' height='100px'> <?php echo $video->title; ?>" value=<?php echo $video->id; ?> data-width="75%">
				<?php echo $video->title; ?></option>
		<?php };

		?>

	</select>


	<input type="radio" name="video_active" value="1" <?php if ("1" == $active ) echo 'checked' ; ?>> Active
	<input type="radio" name="video_active" value="0" <?php if ("0" == $active ) echo 'checked' ; ?>> Inactive

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script type="text/javascript">

		var $ =jQuery.noConflict();
		$(document).ready(function(){

			$('.selectpickerPopularMusic').selectpicker({
				style: 'btn-warning',
				size: 8
			});


		})


        $(document).ready(function(){

            var popularMusic = <?php echo json_encode($popular_music); ?>;

            $('.selectpickerPopularMusic').selectpicker('val',popularMusic);

		})
	</script>

	<?php

}

function save_popular_music_data() {

	global $post;

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;
    global   $newdb;
//	$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "popular_music";
	$query = "SELECT post_id FROM popular_music WHERE post_id=$post->ID";
	$postID=$newdb->get_var($query);

	if($postID!=$post->ID) {
		$newdb->query($newdb->prepare(" INSERT INTO popular_music (post_id,video_id,active)
                                       VALUES (%d,%d,%s)",
			array(
				$post->ID,
				$_POST['popular_music_id'],
				$_POST['video_active'],
			)));
	}else{
		$newdb->update(
			$table_name,
			array(
				'post_id'=>$post->ID,
				'video_id' => $_POST['popular_music_id'],
				'active'=> $_POST['video_active'],
			),
			array( 'post_id' => $post->ID ),
			array('%d','%d','%s'),
			array( '%d' )
		);
	}
}

add_action('save_post_popular_music', 'save_popular_music_data');

//Banner Upload Post Type

function create_banner_post_type() {

	$labels = array(
		'name' =>'Banner',
		'singular_name' =>'Banner',
		'add_new' =>'add Banner',
		'all items' =>'All Banners',
		'add_new_item' =>'Add Banner',
		'edit' =>'Edit Banner',
		'view' =>'View Banner',
	);

	$args=array(
		'labels'=>$labels,
		'public'=>true,
		'rewrite'=>true,
		'supports' => array( 'title' ),
		'menu_position' => 8,
		'menu_icon'   => 'dashicons-format-image',
		'register_meta_box_cb' => 'add_banner_metaboxes'
	);

	register_post_type('banner',$args);

}
add_action('init','create_banner_post_type' );

add_action( 'add_meta_boxes', 'add_banner_metaboxes');

function add_banner_metaboxes() {

	add_meta_box(
		'banner', //id
		'Banner', //title
		'banner_upload', //callbacks
		'banner', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);
	add_meta_box(
		'banner_video', //id
		'Banner Video', //title
		'banner_video', //callbacks
		'banner', //page
		'normal', //position of the div, i.e. (side)
		'default' //priority
	);

}

function banner_video() {

	global $post;
	global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM videos";
	$videos=$newdb->get_results($query);

	$query = "SELECT video_id FROM banners WHERE post_id=$post->ID";
	$banner_video=$newdb->get_var($query);
	$query = "SELECT active FROM banners WHERE post_id=$post->ID";
	$active=$newdb->get_var($query);
	?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<select id="selectpickerBannerVideo" class="selectpickerBannerVideo" name="banner_video_id" data-live-search="true" >

		<?php foreach($videos as $key=>$video){?>
			<option data-content="<img src='<?php echo $video->thumbnail; ?>' width='100px' height='100px'> <?php echo $video->title; ?>" value=<?php echo $video->id; ?> data-width="75%">
				<?php echo $video->title; ?></option>
		<?php };

		?>

	</select>


	<input type="radio" name="video_active" value="1" <?php if ("1" == $active ) echo 'checked' ; ?>> Active
	<input type="radio" name="video_active" value="0" <?php if ("0" == $active ) echo 'checked' ; ?>> Inactive

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script type="text/javascript">

		var $ =jQuery.noConflict();
		$(document).ready(function(){

			$('.selectpickerBannerVideo').selectpicker({
				style: 'btn-warning',
				size: 8
			});


		})

        $(document).ready(function(){

            var bannerVideo = <?php echo json_encode($banner_video); ?>;

            $('.selectpickerBannerVideo').selectpicker('val',bannerVideo);

		})
	</script>

	<?php

}

function save_banner_data() {

	global $post;
	global   $newdb;

	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "banners";
	$query = "SELECT post_id FROM banners WHERE post_id=$post->ID";
	$postID=$newdb->get_var($query);

	if($postID!=$post->ID) {
		$newdb->query($newdb->prepare(" INSERT INTO banners (post_id,video_id,active)
                                       VALUES (%d,%d,%s)",
			array(
				$post->ID,
				$_POST['banner_video_id'],
				$_POST['video_active'],
			)));
	}else{
		$newdb->update(
			$table_name,
			array(
				'video_id' => $_POST['banner_video_id'],
				'active'=> $_POST['video_active'],
			),
			array( 'post_id' => $post->ID ),
			array('%d','%s'),
			array( '%d' )
		);
	}
}

add_action('save_post_banner', 'save_banner_data');

function banner_upload() {

	global $post;
	global   $newdb;
	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$query = "SELECT * FROM banners WHERE post_id=$post->ID";
	$images=$newdb->get_results($query);

	?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/jQuery.filer/js/jquery.filer.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/jQuery.filer/css/jquery.filer.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/thirdbellImage.css">

	<input class="widefat" type="file" name="files[]" id="filer_input" multiple="multiple">
	<div id="imageAdd"></div>

    <?php
	if($images!=''){?>
		<div id="">
			<?php foreach($images as $image){?>
				<a class="updateImage" data-id="<?php echo $image->id ?>">
					<img class="customImage" src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/<?php echo $image->image?>" height="150px" width="160px" onmouseover="this.src='<?php bloginfo('template_url');?>/jQuery.filer/uploads/close-128.png'" onmouseout="this.src='<?php bloginfo('template_url');?>/jQuery.filer/uploads/<?php echo $image->image?>'">
				</a>
			<?php } ?>
		</div>
	<?php }
	?>


	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function(){
			$('#filer_input').filer({
				limit: 5,
				maxSize: 5,
				extensions: ['jpg', 'jpeg', 'png', 'gif'],
				changeInput: true,
				showThumbs: false,
				onRemove: function test(removedFiles){
					//removeFunction(removedFiles);
				},
				uploadFile: {
					url: "<?php bloginfo('template_url'); ?>/jQuery.filer/php/upload.php", //URL to which the request is sent {String}
					data: null, //Data to be sent to the server {Object}
					type: 'POST', //The type of request {String}
					enctype: 'multipart/form-data', //Request enctype {String}
					beforeSend:null, //A pre-request callback function {Function}
					success: function sendlink(response) {

						var temp = JSON.parse(response);
						//console.log(temp.metas[0].name);

						processImage(temp.metas[0].name);

					},
					error: null, //A function to be called if the request fails {Function}
					statusCode: null, //An object of numeric HTTP codes {Object}
					onProgress: null, //A function called while uploading file with progress percentage {Function}
					onComplete: null, //A function called when all files were uploaded {Function}

				}
			});

			function processImage(image_name){

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_add_banner',
						name : image_name,
						post_id: <?php echo json_encode($post->ID);?>
					},
					success: function( image_id ) {
						//console.log(image_id);
						showPicture(image_name,image_id);
					}
				})
			}

			function showPicture(name,id){

				var src="<?php bloginfo('template_url');?>/jQuery.filer/uploads/"+name;
				imageDiv=$('<a class="removeImage" data-id="'+id+'"><span class="imageSpan">'
					+'<img src="'+src+'" width="160px" height="150px" title="Remove">'
				    +'</span></a>');
				$('#imageAdd').append(imageDiv);
			}

			$('#imageAdd').on('click','.removeImage', function() {

				$(this).remove();
				var imageID=$(this).data('id');

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_delete_banner',
						ID : imageID,
					},
					success: function( message ) {
						//console.log(message);
						alert(message);
					}
				})

			});

			$('.updateImage').on('click', function() {

				$(this).remove();
				var imageID=$(this).data('id');

				$.ajax({
					url: ajaxurl,
					type : 'post',
					data : {
						action : 'post_delete_banner',
						ID : imageID,
					},
					success: function( message ) {
						console.log(message);
						alert(message);
					}
				})

			});

		})
	</script>

    <?php

}

add_action( 'wp_ajax_post_add_banner', 'save_banner_image' );

function save_banner_image() {

	$image_name=$_POST['name'];
	$post_id=$_POST['post_id'];
	global   $newdb;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");
		$table_name = "banners";

		$newdb->insert( $table_name, array(
					'post_id' => $post_id,
					'image'=>$image_name,
					'type'=>"main_slider",
				)
			);

		echo $newdb->insert_id;
		die();
	}else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

add_action( 'wp_ajax_post_delete_banner', 'delete_banner_image' );

function delete_banner_image() {

	$image_id=$_POST['ID'];
	global   $newdb;

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {

		//$newdb = new wpdb("root","", "3rdbell_database","localhost");

		$newdb->delete('banners', array('id' => $image_id), array('%d'));

		echo "Image Removed!";
		die();
	}else {
		wp_redirect( get_permalink( $_REQUEST['post_id'] ) );
		exit();
	}
}

//Login Page modification

function thirdbell_login() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/css/login/custom-login-3rdbell.css" />';
}
add_action('login_head', 'thirdbell_login');

//Dashboard modification

function thirdbell_custom_dashboard() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/css/login/thirdbell_admin.css" />';
}

add_action('admin_head', 'thirdbell_custom_dashboard');


//Removing update notifications from dashboard

add_action('after_setup_theme','remove_core_updates');
function remove_core_updates()
{
	add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
	add_filter('pre_option_update_core','__return_null');
	add_filter('pre_site_transient_update_core','__return_null');
	add_filter('pre_site_transient_update_core','remove_core_updates');
	add_filter('pre_site_transient_update_plugins','remove_core_updates');
	add_filter('pre_site_transient_update_themes','remove_core_updates');

}

//Dashboard User limitation for authors

function user_limitation() {

	if ( !current_user_can('manage_options') ) {

		remove_menu_page('tools.php');
		remove_menu_page('edit.php');
		remove_menu_page('profile.php');
		remove_menu_page('edit-comments.php');
		remove_menu_page('upload.php');
	}

	remove_menu_page('plugins.php');
}

add_action( 'admin_init', 'user_limitation' );

//Removing update menu from Dashboard

function edit_admin_menus() {

	global $submenu;
	unset($submenu['index.php'][10]);
	return $submenu;
}
add_action( 'admin_menu', 'edit_admin_menus' );

// Removing screen option and help bar from Dashboard

add_filter( 'contextual_help', 'mytheme_remove_help_tabs', 999, 3 );

function mytheme_remove_help_tabs($old_help, $screen_id, $screen){

	$screen->remove_help_tabs();
	return $old_help;
}

add_filter('screen_options_show_screen', '__return_false');

//Removing visit site link from Dashboard

function remove_admin_bar_links() {

	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('view-site');        // Removes the view site link
	$wp_admin_bar->remove_menu('comments');         // Removes the comments link
	$wp_admin_bar->remove_menu('site-name');         // Removes the site link
	$wp_admin_bar->remove_menu('view');         // Removes the view post link
	$wp_admin_bar->remove_menu('new-content');         // Removes the view post link
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

//Permalink hiding from post page

function hide_permalink() {
	return '';
}
add_filter( 'get_sample_permalink_html', 'hide_permalink' );

function remove_admin_menu_items() {

	$remove_menu_items = array(__('Comments'),__('Pages'),__('Posts'),__('Tools'),__('Media'),__('Appearance'),__('Settings'));
	global $menu;
	end ($menu);
	while (prev($menu)){
		$item = explode(' ',$menu[key($menu)][0]);
		if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
			unset($menu[key($menu)]);}
	}
}

add_action('admin_menu', 'remove_admin_menu_items');

function howdy_message($text) {
	$new_message = str_replace('Howdy', 'Welcome', $text);
	return $new_message;
}

add_filter('gettext', 'howdy_message', 10, 3);

// Removing bulk actions from admin page

add_filter( 'bulk_actions-edit-production_house', 'my_custom_bulk_actions_production_house' );

function my_custom_bulk_actions_production_house( $actions ){

	unset( $actions[ 'edit' ] );
	unset( $actions[ 'delete' ] );
	return $actions;
}

add_filter( 'bulk_actions-edit-serials', 'my_custom_bulk_actions_serials' );

function my_custom_bulk_actions_serials( $actions ){

	unset( $actions[ 'edit' ] );
	unset( $actions[ 'delete' ] );
	return $actions;
}

add_filter( 'bulk_actions-edit-persons', 'my_custom_bulk_actions_persons' );

function my_custom_bulk_actions_persons( $actions ){

	unset( $actions[ 'edit' ] );
	unset( $actions[ 'delete' ] );
	return $actions;
}

add_filter( 'bulk_actions-edit-videos', 'my_custom_bulk_actions_videos' );

function my_custom_bulk_actions_videos( $actions ){

	unset( $actions[ 'edit' ] );
	unset( $actions[ 'delete' ] );
	return $actions;
}

function remove_quick_edit( $actions ) {
	unset($actions['inline hide-if-no-js']);
	return $actions;
}
add_filter('post_row_actions','remove_quick_edit',10,1);

// category creation

add_action('create_term','wp_custom_save_taxonomy');

function wp_custom_save_taxonomy($term_id) {

	$category=get_term_by('id', $term_id, 'category');
	global   $newdb;

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$newdb->query($newdb->prepare(" INSERT INTO categories (name,category_id,parent_id,created_at)
                                       VALUES (%s,%d,%d,%s)",
		array(
			$category->name,
			$category->term_id,
			$category->parent,
			current_time('mysql'),
		)));
}

add_action('edit_term','wp_custom_edit_taxonomy');

function wp_custom_edit_taxonomy($term_id) {

    global   $newdb;
	$category=get_term_by('id', $term_id, 'category');

	//$newdb = new wpdb("root","", "3rdbell_database","localhost");
	$table_name = "categories";
	$newdb->update(
		$table_name,
		array(
			'category_id'=>$category->term_id,
			'name' => $_POST['name'],
			'updated_at'=> current_time('mysql'),
		),
		array( 'category_id' => $category->term_id ),
		array('%d','%s','%s'),
		array( '%d' )
	);
}

add_action('delete_term_taxonomy','wp_custom_delete_taxonomy');

function wp_custom_delete_taxonomy($term_id) {

    global   $newdb;
	$newdb = new wpdb("root","", "3rdbell_database","localhost");
	//$newdb->delete('categories', array('category_id' => $term_id), array('%d'));
}

add_filter( 'gettext', 'change_publish_button', 10, 2 );

function change_publish_button( $translation, $text ) {

		if ( $text == 'Publish' ){
			return 'Save & Publish';
		}
	return $translation;
}


function add_profile_widgets() {

	wp_add_dashboard_widget(
		'dashboard_widget_profile', //widget id
		'Total Profiles', //widget name
		'show_profile_count'); //callback
}

add_action('wp_dashboard_setup', 'add_profile_widgets' );

function show_profile_count( $post, $callback_args ) {

	if ( current_user_can('manage_options') ) {
		?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/login/odometer-theme-train-station.css"/>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/odometer.js"></script>

		<?php
        global   $newdb;
		//echo "Hello World";
		//$newdb = new wpdb("root", "", "3rdbell_database", "localhost");
		$table_name = "persons";
		$query = "SELECT COUNT(*) FROM persons";
		$count = $newdb->get_var($query);
		echo '<div id="odometer odometer_number_one" class="odometer odometer_number_one">0</div>';
	}
}

function add_user_widgets() {

	wp_add_dashboard_widget(
		'dashboard_widget_user', //widget id
		'Total Users', //widget name
		'show_user_count'); //callback
}

add_action('wp_dashboard_setup', 'add_user_widgets' );

function show_user_count( $post, $callback_args ) {

	if ( current_user_can('manage_options') ) {
		?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/login/odometer-theme-train-station.css"/>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/odometer.js"></script>

		<?php

		//echo "Hello World";
		global   $newdb;
		//$newdb = new wpdb("root", "", "3rdbell_database", "localhost");
		$table_name = "persons";
		$query = "SELECT COUNT(*) FROM users";
		$count = $newdb->get_var($query);
		echo '<div id="odometer odometer_number_three" class="odometer odometer_number_three">0</div>';
	}
}

function add_watch_widgets() {

	wp_add_dashboard_widget(
		'dashboard_widget_watch', //widget id
		'Total hours Of View', //widget name
		'show_watch_count'); //callback
}

add_action('wp_dashboard_setup', 'add_watch_widgets' );

function show_watch_count( $post, $callback_args ) {

	if ( current_user_can('manage_options') ) {
		?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/login/odometer-theme-train-station.css"/>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/odometer.js"></script>

		<?php

		//echo "Hello World";
		global   $newdb;
		//$newdb = new wpdb("root", "", "3rdbell_database", "localhost");
		$table_name = "persons";
		$query = "SELECT SUM(time) FROM video_watch_logs;";
		$count = $newdb->get_var($query);
		echo '<div id="odometer odometer_number_four" class="odometer odometer_number_four">0</div>';
	}
}

function add_video_widget() {

	wp_add_dashboard_widget(
		'dashboard_widget_video', //widget id
		'Total Videos', //widget name
		'show_video_count'); //callback
}

add_action('wp_dashboard_setup', 'add_video_widget' );

function show_video_count( $post, $callback_args ) {

	if ( current_user_can('manage_options') ) {
		?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/login/odometer-theme-train-station.css"/>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/odometer.js"></script>

		<?php
        global   $newdb;
		//$newdb = new wpdb("root", "", "3rdbell_database", "localhost");
		$table_name = "videos";
		$query = "SELECT COUNT(*) FROM videos";
		$videos = $newdb->get_var($query);
		$query = "SELECT COUNT(*) FROM persons";
		$persons = $newdb->get_var($query);
		$query = "SELECT COUNT(*) FROM users";
		$users = $newdb->get_var($query);
		$query = "SELECT SUM(time) FROM video_watch_logs;";
		$watch = $newdb->get_var($query);

		echo '<div id="odometer odometer_number_two" class="odometer odometer_number_two">0</div>';
	}
	?>
	<script>
		var $ =jQuery.noConflict();
		$(document).ready(function(){

			videos=<?php echo json_encode($videos);?>;
			persons=<?php echo json_encode($persons);?>;
			users=<?php echo json_encode($users);?>;
			watch=<?php echo json_encode($watch);?>;

			setTimeout(function(){
				$('.odometer_number_one').html(persons);
				$('.odometer_number_two').html(videos);
				$('.odometer_number_three').html(users);
				$('.odometer_number_four').html(watch);
			}, 1000);

		})
	</script>

	<?php
}