<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '3rdbelldb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' n3m0mrr;H0>dAPvAX[rknk9cVN^4=K[vpu(|6~L`io,3[ex%2>A?Ju1v)%4H1]y');
define('SECURE_AUTH_KEY',  'q?ULr:4;q_zM+cmRWuqGu 6RdMg]>k>OB27n$eXgOO`pnr } OH2?*5c-o,5V9BS');
define('LOGGED_IN_KEY',    'Vc]6(sNvb[{S,at9uG7#%s0RB(%%9t)oCfD*e<q[:6b;$}L#sxWr_[dG+^3.laF ');
define('NONCE_KEY',        'hcZGA[CDuR5ootPWAAnw<^-)<QFIcg]XdHvs8mWXmi6Vo{|,Z2N!Q<`n#<~Hr2vN');
define('AUTH_SALT',        '@}9oOG.NdB#.]m?,O~492)>zZmnD5L?-/l7-K=<!oTj$)dhpc_G>K2R<NR9a[-m}');
define('SECURE_AUTH_SALT', 'U5O{{B:Y,z=;wW(8fm;(M#c9t&R4Q@b&eDqaLB7[LqwFz&hdXVye-:/`NzxOrW0}');
define('LOGGED_IN_SALT',   '!AIe.&+4JXEt$}25<%8noPl}]@d]E<cv*<*#@ZM=;u~sXoczN9R3]EL9lXdq{~N-');
define('NONCE_SALT',       'q0K%OS6;f75<<HTV~V8N)DXtc4~=UYXhWJ7]QBwnkH2^IDWQUvbJo(zlBTDHs=b|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define('WP_DEBUG', false);
ini_set('log_errors','On');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
